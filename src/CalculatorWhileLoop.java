import java.util.Scanner;
public class CalculatorWhileLoop {
	public static void main(String[]args){
		Scanner scan = new Scanner(System.in);
		boolean go=true;
		while (go==true){
			System.out.println("What is the first number you want to use?");
			int num1= scan.nextInt();
			System.out.println("What is the second number you want to use?");
			int num2= scan.nextInt();
			Scanner op= new Scanner(System.in);
			System.out.println("Do you want to add, subrtact, multiply, or divide?");
			String operation = op.nextLine();
			if(operation.equals("add")){
				System.out.println(num1+num2);
			}else if (operation.equals("subtract")){
				System.out.println(num1-num2);
			}else if (operation.equals("divide")){
				System.out.println(num1/num2);
			}else if (operation.equals("multiply")){
				System.out.println(num1*num2);
			}
			
			Scanner repeat= new Scanner (System.in);
			System.out.println("Do you want to calculate anything else?");
			String question= repeat.nextLine();
			if(question.equals("No")){
				go=false;
			}
			
		}
	}

}


