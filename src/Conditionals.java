import java.util.Scanner;
public class Conditionals {
	public static void main(String[]args){
		Scanner scan = new Scanner(System.in);
		System.out.print("What is your age?");
		int age = scan.nextInt();
		if(age==16){
			System.out.println("You can drive with a permit.");

		}else if (age<17){
			System.out.println("You are under the age limit. You can't drive");

		}else{
			System.out.println("You can drive!");
		}

	}
}

